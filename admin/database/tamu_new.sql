-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2020 at 01:34 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamu_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id` int(11) NOT NULL,
  `pertanyaan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `pertanyaan`) VALUES
(2, 'Apakah anda puas dengan Pelayanan Desa Kami ? '),
(3, 'Apakah anda puas dengan kebersihan desa kami ?\r\n'),
(4, 'Apakah anda puas fasilitas desa kami ?');

-- --------------------------------------------------------

--
-- Table structure for table `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_berita`
--

INSERT INTO `tb_berita` (`id`, `judul`) VALUES
(2, 'Mari Kita Jaga Keamanan Bersama Dengan Tidak Melayani Pedagang Masuk Ruangan\r\n'),
(3, 'Mari Kita Wujudkan WILAYAH BEBAS dari KORUPSI ');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai2`
--

CREATE TABLE `tb_pegawai2` (
  `id` int(9) NOT NULL,
  `nip` varchar(70) DEFAULT NULL,
  `nama_peg` varchar(255) DEFAULT NULL,
  `id_u_kerja` int(9) DEFAULT NULL,
  `telpon` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pegawai2`
--

INSERT INTO `tb_pegawai2` (`id`, `nip`, `nama_peg`, `id_u_kerja`, `telpon`) VALUES
(538, '232323', 'TRISNAWATI,SP', 100, '6285781480396'),
(539, '65454', 'DEDI SUPARJO. S,Pd.I', 101, '6285781480396'),
(540, '12244', 'AGUS SETIAWAN', 100, '6285781480396');

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile`
--

CREATE TABLE `tb_profile` (
  `id_profile` int(11) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profile`
--

INSERT INTO `tb_profile` (`id_profile`, `nama_perusahaan`, `foto`) VALUES
(1, 'PT. JAYA ABADI', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk`
--

CREATE TABLE `tb_spk` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `pertanyaan` varchar(255) NOT NULL,
  `jawaban` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_spk`
--

INSERT INTO `tb_spk` (`id`, `nama`, `pertanyaan`, `jawaban`) VALUES
(400, 'Drs. Zulkifli, MM', 'Apakah anda puas dengan Pelayanan Desa Kami ?', 'Sangat Puas'),
(401, 'Drs. Zulkifli, MM', 'Apakah anda puas dengan kebersihan desa kami ?', 'Puas'),
(402, 'Drs. Zulkifli, MM', 'Apakah anda puas fasilitas desa kami ?', 'Cukup Puas'),
(403, 'TRISNAWATI,SP', 'Apakah anda puas dengan Pelayanan Desa Kami ?', 'Puas'),
(404, 'TRISNAWATI,SP', 'Apakah anda puas dengan kebersihan desa kami ?', 'Cukup Puas'),
(405, 'TRISNAWATI,SP', 'Apakah anda puas fasilitas desa kami ?', 'Sangat Puas'),
(406, 'PARMAN', 'Apakah anda puas dengan Pelayanan Desa Kami ?', 'Sangat Puas'),
(407, 'PARMAN', 'Apakah anda puas dengan kebersihan desa kami ?', 'Puas'),
(408, 'PARMAN', 'Apakah anda puas fasilitas desa kami ?', 'Cukup Puas'),
(409, 'ADITYA S', 'Apakah anda puas dengan Pelayanan Desa Kami ?', 'Tidak Puas'),
(410, 'ADITYA S', 'Apakah anda puas dengan kebersihan desa kami ?', 'Cukup Puas'),
(411, 'ADITYA S', 'Apakah anda puas fasilitas desa kami ?', 'Puas'),
(412, 'AGUS RAHARJO', 'Apakah anda puas dengan Pelayanan Desa Kami ?', 'Puas'),
(413, 'AGUS RAHARJO', 'Apakah anda puas dengan kebersihan desa kami ?', 'Cukup Puas'),
(414, 'AGUS RAHARJO', 'Apakah anda puas fasilitas desa kami ?', 'Tidak Puas');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tamu`
--

CREATE TABLE `tb_tamu` (
  `id_tamu` int(9) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telp` varchar(30) NOT NULL,
  `jk` varchar(30) NOT NULL,
  `keperluan` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `ketemu` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `ttd` varchar(255) NOT NULL,
  `instansi` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(9) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama`, `username`, `pass`, `level`, `foto`) VALUES
(5, 'admin', 'admin', 'admin', 'admin', 'avatar5.png');

-- --------------------------------------------------------

--
-- Table structure for table `t_u_kerja`
--

CREATE TABLE `t_u_kerja` (
  `id` int(9) NOT NULL,
  `u_kerja` varchar(255) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_u_kerja`
--

INSERT INTO `t_u_kerja` (`id`, `u_kerja`, `ket`) VALUES
(100, 'Keuangan', ''),
(101, 'Kepegawaian', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pegawai2`
--
ALTER TABLE `tb_pegawai2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indexes for table `tb_spk`
--
ALTER TABLE `tb_spk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tamu`
--
ALTER TABLE `tb_tamu`
  ADD PRIMARY KEY (`id_tamu`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_u_kerja`
--
ALTER TABLE `t_u_kerja`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_berita`
--
ALTER TABLE `tb_berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_pegawai2`
--
ALTER TABLE `tb_pegawai2`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=541;

--
-- AUTO_INCREMENT for table `tb_profile`
--
ALTER TABLE `tb_profile`
  MODIFY `id_profile` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_spk`
--
ALTER TABLE `tb_spk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=415;

--
-- AUTO_INCREMENT for table `tb_tamu`
--
ALTER TABLE `tb_tamu`
  MODIFY `id_tamu` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=317;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_u_kerja`
--
ALTER TABLE `t_u_kerja`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
